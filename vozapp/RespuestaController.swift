//
//  RespuestaController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/8/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RespuestaController: UIViewController {
    
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getFiles()
    }

    var respuesta : Respuesta?
    
    let realm = try! Realm()
    
    var nameAssets = [String]()
    var filesTable = [File]()
    
    let assetsView : ViewAssets = {
        let view = ViewAssets()
        return view
    }()
    
    func getFiles() {
        let filter = "answer.id = \((respuesta?.id)!)"
        let files = realm.objects(File.self).filter(filter)
        for file in files {
            nameAssets += [file.filename]
            filesTable += [file]
        }
        
        if nameAssets.count > 0 {
            setupAssetsView()
        }
    }
    
    func setupAssetsView() {
        view.addSubview(assetsView)
        assetsView.titleView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        assetsView.tableAssets.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        assetsView.tableAssets.heightAnchor.constraint(equalToConstant: 150).isActive = true
        assetsView.tableAssets.tableFooterView = UIView()
        assetsView.tableAssets.backgroundColor = .clear
        assetsView.tableAssets.register(AssetCell.self, forCellReuseIdentifier: "assetcell")
        assetsView.deleteButton = false
        assetsView.assets = nameAssets
        assetsView.files = filesTable
        assetsView.frame = CGRect(x: 0, y: 400, width: view.frame.width, height: 225)
    }
    
    func setupView() {
        let backgroud = UIImage(named: "background_tile")
        view.backgroundColor = UIColor(patternImage: backgroud!)
        
        let fil = "id = \(Globals.respuestaId)"
        print(fil)
        let respuestas = realm.objects(Respuesta.self).filter(fil)
        respuesta = respuestas.first
        
        fechaLabel.text = respuesta?.time
        tituloLabel.text = respuesta?.title
        descripcionLabel.text = respuesta?.textual
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
