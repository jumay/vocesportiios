//
//  ReportesTableView.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/31/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class ReportesTableView: UITableView, UITableViewDelegate, UITableViewDataSource {
    
    let cellId = "reporteCell"
    let realm = try! Realm()
    var reports = [Report]()
    var reportsWithAnswers = [Int]()
    
    override init(frame: CGRect, style: UITableViewStyle) {
        super.init(frame: frame, style: style)
        getReports()
        setupView()
    }

    func getReports() {
        reports = []
        let rps = realm.objects(Report.self)
        
        for r in rps {
            reports += [r]
        }
    }
    
    func setupView() {
        delegate = self
        dataSource = self
        tableFooterView = UIView()
        backgroundColor = .clear
        register(ReporteCell.self, forCellReuseIdentifier: cellId)
        separatorInset = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
        separatorStyle = .none

        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reports.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ReporteCell
        let data = reports[indexPath.row]
        let respuestas = realm.objects(Respuesta.self).filter("report.id = \(data.id)")
        var arrayResp = [Respuesta]()
        for r in respuestas {
            arrayResp += [r]
        }
        
        cell.content.widthAnchor.constraint(equalToConstant: self.frame.width - 30).isActive = true
        cell.content.heightAnchor.constraint(equalToConstant: 140).isActive = true
        cell.addConstraint(NSLayoutConstraint(item: cell.content, attribute: .left, relatedBy: .equal, toItem: cell, attribute: .left, multiplier: 1, constant: 15))
        cell.respuestas = arrayResp
        if arrayResp.count > 0 {
            reportsWithAnswers += [indexPath.row]
        }
        cell.respuestasTable.widthAnchor.constraint(equalToConstant: self.frame.width).isActive = true
        cell.respuestasTable.heightAnchor.constraint(equalToConstant: 150).isActive = true
        cell.selectionStyle = .none
        if let catName = data.category?.name {
            cell.labelType.text = catName
        }
        cell.fechaLabel.text = data.time
        cell.reportesController = reportesController
        cell.readMoreButton.tag = indexPath.row
        cell.readMoreButton.addTarget(self, action: #selector(sendToDetalleWithButton(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        /*let index = reportsWithAnswers.index(of: indexPath.row)
        if index != nil {
            return 315
        }else{
            return 150
        }*///Esto hay que cambiarlo cuando se decida el diseño de las respuestas
        
        return 150
    }
    
    var reportesController: ReportesController?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //let rep = reports[indexPath.row]
        //reportesController?.sendToDetalle(report: rep)
    }
    
    func sendToDetalleWithButton(sender: UIButton) {
        let rep = reports[sender.tag]
        reportesController?.sendToDetalle(report: rep)
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
