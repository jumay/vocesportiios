//
//  ViewForm.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/26/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class ViewForm: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    let labelForm: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.text = "formReport"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let valueLabel: UIButton = {
        let label = UIButton()
        label.setTitle("Seleccionar", for: .normal)
        label.setTitleColor(.white, for: .normal)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let btnDropdown: UIImageView = {
        let img = UIImageView()
        img.image = #imageLiteral(resourceName: "dropdwonIcon")
        img.translatesAutoresizingMaskIntoConstraints = false
        return img
    }()
    
    let fieldWhite: UITextView = {
        let field = UITextView()
        field.textColor = Constants().blueColor
        field.text = "Descripción"
        field.backgroundColor = Constants().grayColor
        field.font = UIFont.systemFont(ofSize: 15)
        field.autocorrectionType = UITextAutocorrectionType.no
        field.keyboardType = UIKeyboardType.default
        field.returnKeyType = UIReturnKeyType.done
        field.layer.cornerRadius = 5
        return field
    }()
    
    func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 60).isActive = true
        
        //addSubview(labelForm)
        //addConstraint(NSLayoutConstraint(item: labelForm, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        //addConstraint(NSLayoutConstraint(item: labelForm, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
        
       
    }
    
    
    func setImageDrop() {
        addSubview(btnDropdown)
        
        btnDropdown.widthAnchor.constraint(equalToConstant: 40).isActive = true
        btnDropdown.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        addConstraint(NSLayoutConstraint(item: btnDropdown, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -10))
        
        addConstraint(NSLayoutConstraint(item: btnDropdown, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
    }
    
    func setValueLabel() {
        addSubview(valueLabel)
        
        valueLabel.widthAnchor.constraint(equalToConstant: 350).isActive = true
        valueLabel.heightAnchor.constraint(equalToConstant: 65).isActive = true
        valueLabel.contentHorizontalAlignment = .left
        valueLabel.setTitleColor(Constants().blueColor, for: .normal)
        
        addConstraint(NSLayoutConstraint(item: valueLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: valueLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
    }
    
    func setTextField() {
        addSubview(fieldWhite)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
