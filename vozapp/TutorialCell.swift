//
//  TutorialCell.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/10/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class TutorialCell: UICollectionViewCell {
    @IBOutlet weak var imageTutorial: UIImageView!
}
