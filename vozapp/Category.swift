//
//  Category.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/23/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import RealmSwift
import Realm


class Category: Object {
    dynamic var id = Int()
    dynamic var name = String()
    
    func save() {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(self)
        }
    }
    
    func delete(id: String) {
        let realm = try! Realm()
        
        let filter = "id = \(id)"
        let element = realm.objects(Category.self).filter(filter).first
        
        if (element != nil) {
            try! realm.write {
                realm.delete(element!)
            }
        }
    }
}
