//
//  Preference.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/23/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import RealmSwift
import Realm

class Preference: Object {
    dynamic var id = Int()
    dynamic var name = String()
    dynamic var sexo = String()
    dynamic var edad = String()
    dynamic var hijos = String()
    dynamic var anonimo = Bool()
    dynamic var uid = Int()
    dynamic var nombres = String()
    dynamic var apellidos = String()
    dynamic var telefono = String()
    dynamic var registered = Bool()
    dynamic var tutorial = Bool()
    dynamic var departamento: Department?
    dynamic var municipaidad: Municipality?
    let escuelas = List<School>()
    
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func save() {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(self)
        }
    }
    
    func delete(id: String) {
        let realm = try! Realm()
        
        let filter = "id = \(id)"
        let element = realm.objects(Preference.self).filter(filter).first
        
        if (element != nil) {
            try! realm.write {
                realm.delete(element!)
            }
        }
    }
}
