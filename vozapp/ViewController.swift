//
//  ViewController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/10/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var viewTutorial: UIImageView!
    @IBOutlet weak var collectionview: UICollectionView!

    @IBOutlet weak var labelTutorial: UILabel!
    @IBOutlet weak var tutorialContenedor: UIView!
    let cellId = "cellCollection"
    let realm = try! Realm()
    let images = ["03_Tutorial-1","03_Tutorial-2","03_Tutorial-3","03_Tutorial-4","03_Tutorial-5","03_Tutorial-6"]
    var pref = Preference()
    var tutorial = false
    let connection = Connection().isInternetAvailable()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Services().tutorialController = self
        
        if connection == false {
            noConection()
        }
        
        let preferences = realm.objects(Preference.self)
        if  preferences.count > 0{
            pref = preferences.first!
            if pref.tutorial == true {
                viewTutorial?.frame = CGRect(x: 9, y: 5, width: 37, height: 30)
                viewTutorial?.image = #imageLiteral(resourceName: "checkedBlue")
            }else{
                performSegue(withIdentifier: "tutorialExist", sender: self)
                viewTutorial?.frame = CGRect(x: 9, y: 5, width: 30, height: 30)
                viewTutorial?.image = #imageLiteral(resourceName: "unchecked2")
            }
            
        }
        
        tutorialContenedor?.isHidden = true
        labelTutorial?.isHidden = true
        viewTutorial?.isHidden = true

        
        collectionview?.delegate = self
        collectionview?.dataSource = self
        setupCollectionView()
        setupNavigationButtons()
        setupButtonTutorial()
    }
    
    var alertViewResponder: SCLAlertViewResponder?
    
    func chargeCatalogs() {
        alertViewResponder = SCLAlertView().showWarning("Configurando tus datos", subTitle: "Esto tomara unos cuantos segundos")
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func stopLoad() {
        alertViewResponder?.close() // Close view
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    func setupButtonTutorial() {
        let tapTutorial = UITapGestureRecognizer()
        tapTutorial.addTarget(self, action: #selector(changeTutorial))
        
        viewTutorial?.isUserInteractionEnabled = true
        viewTutorial?.addGestureRecognizer(tapTutorial)
    }
    
    func changeTutorial() {
        if tutorial == true {
            tutorial = false
            viewTutorial?.frame = CGRect(x: 9, y: 5, width: 30, height: 30)
            viewTutorial?.image = #imageLiteral(resourceName: "unchecked2")
        }else{
            tutorial = true
            viewTutorial?.frame = CGRect(x: 9, y: 5, width: 37, height: 30)
            viewTutorial?.image = #imageLiteral(resourceName: "checkedBlue")
        }
    }
    
    func noConection () {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showError("No estas conectado a internet", subTitle: "Necesitas una conexión a internet para poder registrarte")
    }

    func setupCollectionView() {
        if let flowLayout = collectionview?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.minimumLineSpacing = 0
            flowLayout.itemSize = CGSize(width: view.frame.width, height: view.frame.height - 25)
            flowLayout.scrollDirection = .horizontal
            
        }
        collectionview?.backgroundColor = .clear
        collectionview?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionview?.contentInset = UIEdgeInsetsMake(30, 30, 30, 40)
        collectionview?.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionview?.isPagingEnabled = true
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let width = Int(view.frame.width)
        let pointer = CGFloat(width)

        if targetContentOffset.pointee.x >= pointer {
            tutorialContenedor?.isHidden = false
            labelTutorial?.isHidden = false
            viewTutorial?.isHidden = false
            
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }else{
            tutorialContenedor?.isHidden = true
            labelTutorial?.isHidden = true
            viewTutorial?.isHidden = true
            
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    func setupNavigationButtons() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Cerrar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(senderTutorial))
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func senderTutorial() {
        if connection == false {
            noConection()
        }else{
            let realm = try! Realm()
            
            let preferences = realm.objects(Preference.self)
            
            
            if preferences.count <= 0 {
                performSegue(withIdentifier: "tutorialFirst", sender: self)
            }else{
                if (preferences[0].registered == true) {
                    performSegue(withIdentifier: "tutorialExist", sender: self)
                }else{
                    performSegue(withIdentifier: "tutorialFirst", sender: self)
                }
                
                let lastPreference = realm.objects(Preference.self).first
                let pref = Preference()
                pref.sexo = (lastPreference?.sexo)!
                pref.edad = (lastPreference?.edad)!
                pref.hijos = (lastPreference?.hijos)!
                pref.departamento = lastPreference?.departamento
                pref.municipaidad = lastPreference?.municipaidad
                pref.anonimo = (lastPreference?.anonimo)!
                pref.uid = (lastPreference?.uid)!
                pref.registered = (lastPreference?.registered)!
                pref.tutorial = self.tutorial
                
                try! realm.write {
                    realm.add(pref, update: true)
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath as IndexPath)
        let view = UIImageView()
        let imageName = images[indexPath.row]
        view.image = UIImage(named: imageName)
        cell.backgroundView = view
        
        return cell
    }
    
}

