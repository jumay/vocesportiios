//
//  EstablecimientosViewController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/19/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class EstablecimientosViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var titleView: UIView!
    let cellId = "EstablecimentoCellId"
    var schoolforRemove = ""
    @IBOutlet weak var tableView: UITableView!
    
    let realm = try! Realm()
    var schools = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
        setupView()
        setupNavigationButtons()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let addButton: UIButton = {
        let btn = UIButton()
        btn.setTitle("  + Agregar Nuevo", for: .normal)
        btn.setTitleColor(Constants().blueColor, for: .normal)
        btn.layer.cornerRadius = 5
        btn.contentHorizontalAlignment = .left
        btn.addTarget(self, action: #selector(showAlert), for: UIControlEvents.touchUpInside)
        btn.backgroundColor = Constants().grayColor
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    func showAlert () {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "moreIcon")
        let txt = alertView.addTextField("Nombre del establecimiento")
        alertView.addButton("Guardar") {
            if txt.text != "" {
                self.schools.append(txt.text!)
                self.tableView.reloadData()
            }
        }
        
        alertView.showWarning("Agregar establecimiento", subTitle: "Ingresa el nombre del establecimiento que quieres agregar",closeButtonTitle: "Cancelar" ,circleIconImage: alertViewIcon)
        
    }
    
    func setupView() {
        view.addSubview(addButton)
        
        addButton.widthAnchor.constraint(equalToConstant: self.view.frame.width - 30).isActive = true
        addButton.heightAnchor.constraint(equalToConstant: titleView.frame.height).isActive = true
        
        view.addConstraint(NSLayoutConstraint(item: addButton, attribute: .top, relatedBy: .equal, toItem: titleView, attribute: .top, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: addButton, attribute: .centerX, relatedBy: .equal, toItem: titleView, attribute: .centerX, multiplier: 1, constant: 0))
        
    }
    
    func setupNavigationButtons() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Siguiente", style: UIBarButtonItemStyle.plain, target: self, action: #selector(setupUserInformation))
    }
    
    func setupUserInformation() {
        
        performSegue(withIdentifier: "typeUserSelect", sender: self)
        
        /*let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "dropdown_arrow")
        alertView.addButton("Anónimo", target: self, selector: #selector(anonimo))
        alertView.addButton("Información personal", target: self, selector: #selector(informacionPersonal))
        alertView.showInfo("¿Cómo quisiera reportar: anónimamente o brindando información personal?", subTitle: "" ,circleIconImage: alertViewIcon)*/
    }
    
    func anonimo() {
        let preferences = realm.objects(Preference.self)
        let pref = Preference()
        pref.anonimo = true
        
        if preferences.count <= 0 {
            pref.save()
        }else{
            pref.sexo = preferences[0].sexo
            pref.edad = preferences[0].edad
            pref.hijos = preferences[0].hijos
            pref.departamento = preferences[0].departamento
            pref.municipaidad = preferences[0].municipaidad
            pref.uid = preferences[0].uid
            pref.registered = false
            try! realm.write {
                realm.add(pref, update: true)
            }
        }
        
        let saveS = saveSchools()
        
        if saveS {
            Services().postUser()
        }
        
        performSegue(withIdentifier: "usuarioAnonimo", sender: self)
    }
    
    func informacionPersonal() {
        let preferences = realm.objects(Preference.self)
        let pref = Preference()
        pref.anonimo = false
        
        if preferences.count <= 0 {
            pref.save()
        }else{
            pref.sexo = preferences[0].sexo
            pref.edad = preferences[0].edad
            pref.hijos = preferences[0].hijos
            pref.departamento = preferences[0].departamento
            pref.municipaidad = preferences[0].municipaidad
            pref.uid = preferences[0].uid
            pref.registered = false
            try! realm.write {
                realm.add(pref, update: true)
            }
        }
        let saveS = saveSchools()
        
        if saveS {
            performSegue(withIdentifier: "informacionPersonal", sender: self)
        }
    }
    
    func saveSchools() -> Bool {
        for school in schools {
            let sc = School()
            
            sc.name = school
            sc.save()
        }
    
        return true
    }

    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.allowsSelection = false
    }
    
    func removeSchool(_ sender: UIButton) {
        schoolforRemove = schools[sender.tag]
        let index = schools.index(of: schoolforRemove)
        schools.remove(at: index!)
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! EstablecimientoCell
        cell.labelName.text = schools[indexPath.row]
        cell.removeButton?.tag = indexPath.row
        cell.content.layer.cornerRadius = 5
        cell.removeButton?.addTarget(self, action: #selector(removeSchool(_:)), for: UIControlEvents.touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55.0
    }
}
