//
//  ViewFormTransparent.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/30/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class ViewFormTransparent: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    let fieldTransparent: UITextField = {
        let field = UITextField()
        //field.placeholder = "Escribir Lugar"
        field.textColor = Constants().blueColor
        field.font = UIFont.systemFont(ofSize: 15)
        field.borderStyle = UITextBorderStyle.none
        field.backgroundColor = Constants().grayColor
        field.autocorrectionType = UITextAutocorrectionType.no
        field.keyboardType = UIKeyboardType.default
        field.returnKeyType = UIReturnKeyType.done
        field.clearButtonMode = UITextFieldViewMode.whileEditing
        field.layer.cornerRadius = 5
        field.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        field.attributedPlaceholder = NSAttributedString(string: "Escribir Lugar", attributes: [NSForegroundColorAttributeName: Constants().blueColor])
        return field
    }()
    
    let fieldFechaHora: UITextField = {
        let field = UITextField()
        //field.placeholder = "Escribir Lugar"
        field.textColor = .white
        field.font = UIFont.systemFont(ofSize: 15)
        field.borderStyle = UITextBorderStyle.none
        field.backgroundColor = Constants().grayColor
        field.autocorrectionType = UITextAutocorrectionType.no
        field.keyboardType = UIKeyboardType.default
        field.returnKeyType = UIReturnKeyType.done
        field.clearButtonMode = UITextFieldViewMode.whileEditing
        field.layer.cornerRadius = 5
        field.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        field.attributedPlaceholder = NSAttributedString(string: "Fecha y Hora del Hecho", attributes: [NSForegroundColorAttributeName: Constants().blueColor])
        return field
    }()
    
    let placeButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "locationIcon"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 28).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        return btn
    }()
    
    let dateButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "calendarIcon"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        return btn
    }()
    
    let emptyDateButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "ic_delete"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 35).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 35).isActive = true
        return btn
    }()
    
    let cameraButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "cameraIcon"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btn.backgroundColor = Constants().yellowColor
        btn.layer.cornerRadius = 25
        btn.imageEdgeInsets = UIEdgeInsetsMake(15,12,15,12)
        btn.clipsToBounds = true
        return btn
    }()
    
    let pictureButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "galeryIcon"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btn.backgroundColor = Constants().yellowColor
        btn.layer.cornerRadius = 25
        btn.imageEdgeInsets = UIEdgeInsetsMake(15,12,15,12)
        btn.clipsToBounds = true
        return btn
    }()
    
    let audioButton: UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "audioIcon"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btn.backgroundColor = Constants().yellowColor
        btn.layer.cornerRadius = 25
        btn.imageEdgeInsets = UIEdgeInsetsMake(12,17,12,17)
        btn.clipsToBounds = true
        return btn
    }()
    
    
    
    let assetsLabel: UILabel = {
        let label = UILabel()
        label.textColor = Constants().blueColor
        label.text = "Adjuntar Archivos"
        label.font = UIFont.systemFont(ofSize: 15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    func setupView() {
        translatesAutoresizingMaskIntoConstraints = false
        //heightAnchor.constraint(equalToConstant: 40).isActive = true
        
    }
    
    func setTextField() {
        addSubview(fieldTransparent)
        addSubview(placeButton)
    
        addConstraint(NSLayoutConstraint(item: placeButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: placeButton, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -15))
    }
    
    func setFechaField() {
        addSubview(fieldFechaHora)
        addSubview(dateButton)
        //addSubview(emptyDateButton)
        
        //addConstraint(NSLayoutConstraint(item: emptyDateButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        //addConstraint(NSLayoutConstraint(item: emptyDateButton, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -15))
        
        addConstraint(NSLayoutConstraint(item: dateButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: dateButton, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -10))
    }
    
    func setAssetsView() {
        addSubview(assetsLabel)
        addSubview(cameraButton)
        addSubview(pictureButton)
        addSubview(audioButton)
        
        addConstraint(NSLayoutConstraint(item: assetsLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: assetsLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 15))
        
        addConstraint(NSLayoutConstraint(item: audioButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: audioButton, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: 12))
    
        addConstraint(NSLayoutConstraint(item: pictureButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: pictureButton, attribute: .right, relatedBy: .equal, toItem: audioButton, attribute: .left, multiplier: 1, constant: -5))
        
        addConstraint(NSLayoutConstraint(item: cameraButton, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: cameraButton, attribute: .right, relatedBy: .equal, toItem: pictureButton, attribute: .left, multiplier: 1, constant: -5))
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

