//
//  RespuestaCell.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/8/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class RespuestaCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
    }
    
    
    let readMoreLabel: UILabel = {
        let label = UILabel()
        label.text = "Leer Más"
        label.textColor = .white
        return label
    }()
    
    let labelType: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byTruncatingMiddle
        label.numberOfLines = 0
        label.text = "Categoria del reporte"
        label.textColor = .white
        return label
    }()
    
    let fechaLabel: UILabel = {
        let label = UILabel()
        label.text = "Fecha de envio del reporte"
        label.textColor = .white
        return label
    }()
    
    func setupCell() {
        addSubview(readMoreLabel)
        addSubview(labelType)
        addSubview(fechaLabel)
        
        readMoreLabel.translatesAutoresizingMaskIntoConstraints = false
        labelType.translatesAutoresizingMaskIntoConstraints = false
        fechaLabel.translatesAutoresizingMaskIntoConstraints = false
        
        labelType.widthAnchor.constraint(equalToConstant: 250).isActive = true
        readMoreLabel.heightAnchor.constraint(equalToConstant: self.frame.height).isActive = true
        addConstraint(NSLayoutConstraint(item: readMoreLabel, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -15))
        addConstraint(NSLayoutConstraint(item: readMoreLabel, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: fechaLabel, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 13))
        addConstraint(NSLayoutConstraint(item: labelType, attribute: .top, relatedBy: .equal, toItem: fechaLabel, attribute: .bottom, multiplier: 1, constant: 3))
        
        
        addConstraint(NSLayoutConstraint(item: labelType, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: fechaLabel, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
