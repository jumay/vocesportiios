//
//  Constants.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/17/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    /*let url = "http://vocesporti.mineduc.gob.gt:8080/voces-backend/rs"
    let filesUrl = "http://vocesporti.mineduc.gob.gt:8080/voces-backend"*/
    
    let url = "http://35.164.2.245:8081/voces-backend/rs"
    let filesUrl = "http://35.164.2.245:8081/voces-backend"
    
    let yellowColor = UIColor(red: 255.0/255.0, green: 217/255.0, blue: 2/255.0, alpha: 1)
    let yellowOpaque = UIColor(red: 254/255.0, green: 236/255.0, blue: 148/255.0, alpha: 1)
    let blueColor = UIColor(red: 39/255.0, green: 34/255.0, blue: 97/255.0, alpha: 1)
    let blueOpaque = UIColor(red: 13/255.0, green: 114/255.0, blue: 155/255.0, alpha: 1)
    let grayColor = UIColor(red: 214/255.0, green: 214/255.0, blue: 216/255.0, alpha: 1)
 }
