//
//  FileElement.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/7/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import Realm
import RealmSwift

class File: Object {
    dynamic var id = Int()
    dynamic var filename = String()
    dynamic var url = String()
    dynamic var filetype = String()
    dynamic var answer: Respuesta?
    dynamic var report: Report?
    
    func save() {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(self)
        }
    }
    
    func delete(id: String) {
        let realm = try! Realm()
        
        let filter = "id = \(id)"
        let element = realm.objects(File.self).filter(filter).first
        
        if (element != nil) {
            try! realm.write {
                realm.delete(element!)
            }
        }
    }
}
