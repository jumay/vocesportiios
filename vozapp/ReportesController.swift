//
//  ReportesController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/23/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import CoreLocation
import Alamofire
import AVFoundation

class ReportesController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UIImagePickerControllerDelegate,UINavigationControllerDelegate, CLLocationManagerDelegate, UITextFieldDelegate, UIScrollViewDelegate, AVAudioPlayerDelegate, AVAudioRecorderDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var collectionView: UICollectionView!
    let realm = try! Realm()
    var locationManager: CLLocationManager! = nil
    var pref: Preference?
    var newR = Report()
    var reportToDetail = Report()
    var respuestaSelected = Respuesta()
    
    var deps = [String]()
    var dep = String()
    var muns = [String]()
    var mun = String()
    var categories = [String]()
    var category = String()
    var pickerSelected = 0
    var selectedPickerTag = 0
    var latitude = String()
    var longitude = String()
    var urlImage: URL?
    var urls = [URL]()
    var dataAssets = [Data]()
    let cellId = "cellReportes"
    var date = Date()
    var nameAssets = [String]()
    let datePicker = UIDatePicker()
    var soudRecorder : AVAudioRecorder!
    var soundPlayer : AVAudioPlayer!
    var respuestasCell = [Respuesta]()
    let con = Connection().isInternetAvailable()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Services().reportesController = self
        
        if con == false {
            noConection()
        }
        
        if let pref = realm.objects(Preference.self).first {
            if ((pref.departamento?.name) != nil) {
                dep = (pref.departamento?.name)!
            }
            if ((pref.municipaidad?.name) != nil) {
                mun = (pref.municipaidad?.name)!
            }

        }
        
        muns = []
        let filter = "department.name = '\(dep)'"
        let municipios = realm.objects(Municipality.self).filter(filter)
        
        
        for mn in municipios {
            muns += [mn.name]
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        collectionView?.delegate = self
        collectionView?.dataSource = self
        setupCollectionView()
        //setupView()
        setupMenuBar()
        setupNavigationButtons()
        setupPickerView()
        setCategories()
        setDepartments()
        datePickerSetup()
        setupAssetsView()
        setupRecorder()
    }
    
    var alertViewResponder: SCLAlertViewResponder?
    
    
    func chargeCatalogs() {
        alertViewResponder = SCLAlertView().showWarning("Configurando tus datos", subTitle: "Esto tomara unos cuantos segundos")
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    
    
    func stopLoad() {
        alertViewResponder?.close() // Close view
        UIApplication.shared.endIgnoringInteractionEvents()
    }

    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 64{
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print(keyboardSize.height)
            if self.view.frame.origin.y != 64{
                self.view.frame.origin.y = 64
            }
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if( text == "\n") {
            formReporte.descriptionView.fieldWhite.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if (formReporte.descriptionView.fieldWhite.text == "Descripción") {
            formReporte.descriptionView.fieldWhite.text = ""
        }
        
        formReporte.descriptionView.fieldWhite.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if (formReporte.descriptionView.fieldWhite.text == "") {
            formReporte.descriptionView.fieldWhite.text = "Descripción"
        }
        
        formReporte.descriptionView.fieldWhite.resignFirstResponder()
    }
    
    func setCategories() {
        
        let catego = realm.objects(Category.self)
        
        for ct in catego {
            categories += [ct.name]
        }
    }
    
    func setDepartments() {
        let departamentos = realm.objects(Department.self)
        
        for dep in departamentos {
            deps += [dep.name]
        }
    }
    
    lazy var menuBar: MenuBar = {
        let bar = MenuBar()
        bar.reportesController = self
        bar.translatesAutoresizingMaskIntoConstraints = false
        return bar
    }()
    
    let formReporte: NuevoReporte = {
        let reportView = NuevoReporte()
        reportView.translatesAutoresizingMaskIntoConstraints = false
        return reportView
    }()
    
    lazy var tableReportes: ReportesTableView = {
        let table = ReportesTableView()
        table.reportesController = self
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    let picker : pickerInfo = {
        let picker = pickerInfo()
        return picker
    }()
    
    let buttonSelect: UIButton = {
        let btn = UIButton()
        btn.setTitle("Seleccionar", for: .normal)
        btn.setTitleColor(.blue, for: .normal)
        btn.addTarget(self, action: #selector(actionSelect), for: UIControlEvents.touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let selectPickView: UIView = {
        let viewSelect = UIView()
        viewSelect.backgroundColor = .white
        viewSelect.translatesAutoresizingMaskIntoConstraints = false
        return viewSelect
    }()
    
    let assetsView : ViewAssets = {
        let view = ViewAssets()
        return view
    }()
    
    let buttonDeleteAll : UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btn.widthAnchor.constraint(equalToConstant: 50).isActive = true
        btn.layer.cornerRadius = 25
        btn.backgroundColor = Constants().yellowColor
        btn.setImage(#imageLiteral(resourceName: "trashIcon"), for: .normal)
        btn.contentEdgeInsets = UIEdgeInsetsMake(12,15,12,15)
        btn.clipsToBounds = true
        btn.addTarget(self, action: #selector(deleteAll), for: .touchUpInside)
        return btn
    }()
    
    func deleteAll() {
        
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "dropdown_arrow")
        alertView.addButton("Eliminar") {
            Services().deleteAllReport()
            let allreports = self.realm.objects(Report.self)
            try! self.realm.write {
                self.realm.delete(allreports)
            }
            
            self.tableReportes.reports = []
            self.tableReportes.reloadData()
        }
        
        alertView.showError("¿Seguro?", subTitle: "Al eliminar los reportes no podrás volver a tener acceso a ellos",closeButtonTitle: "Cancelar" ,circleIconImage: alertViewIcon)
        

    }
    
    func showAlertDep() {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showError("Tienes que seleccionar un departamento", subTitle: "")
    }
    
    func charginCategories() {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showWarning("Estamos cargando las categorías", subTitle: "Este proceso tardará unos cuantos segundos")
    }
    
    let tableAss : UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameAssets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "assetcell", for: indexPath) as! AssetCell
        cell.nameAsset.text = nameAssets[indexPath.row]
        cell.selectionStyle = .none
        cell.addDeleteButton()
        cell.buttonDelete.tag = indexPath.row
        cell.buttonDelete.isUserInteractionEnabled = true
        cell.buttonDelete.addTarget(self, action: #selector(deleteAction(_:)), for: UIControlEvents.touchUpInside)
        
        return cell
    }
    
    func deleteAction(_ sender: UIButton) {
        print(sender.tag)
        nameAssets.remove(at: sender.tag)
        tableAss.reloadData()
    }
    
    func setupPickerView() {
        self.view.addSubview(picker)
        self.view.addSubview(selectPickView)
        selectPickView.addSubview(buttonSelect)
        
        buttonSelect.widthAnchor.constraint(equalToConstant: 100).isActive = true
        buttonSelect.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        picker.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        picker.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        selectPickView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        selectPickView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        view.addConstraint(NSLayoutConstraint(item: picker, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0))
        view.addConstraint(NSLayoutConstraint(item: selectPickView, attribute: .top, relatedBy: .equal, toItem: picker, attribute: .top, multiplier: 1, constant: 0))
        
        selectPickView.addConstraint(NSLayoutConstraint(item: buttonSelect, attribute: .right, relatedBy: .equal, toItem: selectPickView, attribute: .right, multiplier: 1, constant: -10))
        selectPickView.addConstraint(NSLayoutConstraint(item: buttonSelect, attribute: .centerY, relatedBy: .equal, toItem: selectPickView, attribute: .centerY, multiplier: 1, constant: 0))
        
        selectPickView.isHidden = true
        picker.isHidden = true
    }
    
    func actionSelect(sender:UIButton!) {
        var pickerRow = 0
        let selectedRow = picker.selectedRow
        let pickerLenght = picker.data.count
        
        if selectedRow > pickerLenght {
            pickerRow = pickerLenght - 1
        }else{
            pickerRow = selectedRow
        }
        
        let newDat = picker.data[pickerRow]
        
        picker.selectedValue = "[Seleccionar]"
        
        if selectedPickerTag == 1 {
            category = newDat
            formReporte.categoryView.valueLabel.setTitle(newDat, for: .normal)
            picker.data = categories
        }else if selectedPickerTag == 2{
            
            if newDat != dep {
                let munEmpty = "Seleccione un Municipio"
                mun = munEmpty
                formReporte.municipalidadView.valueLabel.setTitle(munEmpty, for: .normal)
            }
            
            dep = newDat
            formReporte.departamentoView.valueLabel.setTitle(newDat, for: .normal)
            picker.data = deps
            
            muns = []
            let filter = "department.name = '\(dep)'"
            let municipios = realm.objects(Municipality.self).filter(filter)
            
            for mn in municipios {
                muns += [mn.name]
            }
        }else if selectedPickerTag == 3{
            mun = newDat
            formReporte.municipalidadView.valueLabel.setTitle(newDat, for: .normal)
            picker.data = muns
        }
        
        picker.isHidden = true
        selectPickView.isHidden = true
    }
    
    private func setupMenuBar() {
        view.addSubview(menuBar)
        
        menuBar.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        menuBar.heightAnchor.constraint(equalToConstant: 70).isActive = true
        
        view.addConstraint(NSLayoutConstraint(item: menuBar, attribute: .bottom, relatedBy: .equal, toItem: collectionView, attribute: .top, multiplier: 1, constant: 75))
        
    }
    
    func setupNavigationButtons() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Tutorial", style: UIBarButtonItemStyle.plain, target: self, action: #selector(sendToTutorial))
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Guardar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(saveReport))
    }
    
    func sendToTutorial() {
        performSegue(withIdentifier: "sendToTutorial", sender: self)
    }
    
    func setupCollectionView() {
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.minimumLineSpacing = 0
            flowLayout.itemSize = CGSize(width: view.frame.width, height: view.frame.height - 100)
            flowLayout.scrollDirection = .horizontal
        }
        collectionView?.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.contentInset = UIEdgeInsetsMake(50, 0, 0, 0)
        collectionView?.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, 0, 0)
        collectionView?.isPagingEnabled = true
    }
    
    func showPicker(_ sender:UIButton) {
        
        if selectedPickerTag != 0 {
            
        }else{
        
        }
        selectedPickerTag = sender.tag
        if sender.tag == 1 {
            if categories.count == 0 {
                setCategories()
                charginCategories()
            }else{
                picker.data = categories
            }
        }else if sender.tag == 2{
            picker.data = deps
            
            muns = []
            let filter = "department.name = '\(dep)'"
            let municipios = realm.objects(Municipality.self).filter(filter)
            
            for mn in municipios {
                muns += [mn.name]
            }
        }else{
            picker.data = muns
        }
        
        if sender.tag == 3 {
            if dep == "" {
                showAlertDep()
            }else{
                picker.reloadAllComponents()
                selectPickView.isHidden = false
                picker.isHidden = false
            }
        }else{
            picker.reloadAllComponents()
            selectPickView.isHidden = false
            picker.isHidden = false
        }
    }
    
    func setupView() {
        let backgroud = UIImage(named: "background_tile")
        view.backgroundColor = UIColor(patternImage: backgroud!)
    }

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if targetContentOffset.pointee.x > 0{
            menuBar.selected = 1
            getReloadTable()
        }else{
            menuBar.selected = 0
        }
        menuBar.collectionView.reloadData()
    }
    
    func scrollToMenuItem (menuIndex: Int) {
        
        let indexPath = IndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath, at: [], animated: true)
        if menuIndex == 1 {
            getReloadTable()
        }
    }
    
    func getReloadTable() {
        tableReportes.getReports()
        tableReportes.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    let scrooView: UIScrollView = {
        let view = UIScrollView()
        return view
    }()
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath as IndexPath)
        if indexPath.row == 0 {
            cell.addSubview(scrooView)
            scrooView.delegate = self
            tableAss.delegate = self
            tableAss.dataSource = self
            scrooView.frame = view.bounds
            scrooView.contentSize = CGSize(width: view.frame.width, height: 850)
            scrooView.addSubview(formReporte)
            scrooView.addSubview(tableAss)
            
            //Se genera la vista para el formulario de generar los reportes
            formReporte.widthAnchor.constraint(equalToConstant: view.frame.width - 30).isActive = true
            formReporte.heightAnchor.constraint(equalToConstant: view.frame.height).isActive = true
            
            formReporte.categoryView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 30).isActive = true
            formReporte.categoryView.valueLabel.tag = 1
            formReporte.categoryView.valueLabel.addTarget(self, action: #selector(showPicker(_:)), for: UIControlEvents.touchUpInside)
            
            formReporte.departamentoView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 30).isActive = true
            formReporte.departamentoView.valueLabel.tag = 2
            formReporte.departamentoView.valueLabel.setTitle(dep, for: UIControlState())
            formReporte.departamentoView.valueLabel.addTarget(self, action: #selector(showPicker(_:)), for: UIControlEvents.touchUpInside)
            
            formReporte.municipalidadView.widthAnchor.constraint(equalToConstant: self.view.frame.width - 30).isActive = true
            formReporte.municipalidadView.valueLabel.setTitle(mun, for: UIControlState())
            formReporte.municipalidadView.valueLabel.tag = 3
            formReporte.municipalidadView.valueLabel.addTarget(self, action: #selector(showPicker(_:)), for: UIControlEvents.touchUpInside)
            
            formReporte.descriptionView.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
            
            formReporte.descriptionView.fieldWhite.frame = CGRect(x: 15, y: 0, width: self.view.frame.width - 30, height: 120)
            formReporte.descriptionView.fieldWhite.delegate = self
            
            formReporte.viewLugar.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
            formReporte.viewLugar.fieldTransparent.frame = CGRect(x: 15, y: 0, width: (self.view.frame.width / 10) * 8 , height: 40)
            formReporte.viewLugar.placeButton.addTarget(self, action: #selector(accessLocation), for: UIControlEvents.touchUpInside)
            formReporte.viewLugar.fieldTransparent.delegate = self
            
            formReporte.viewFecha.widthAnchor.constraint(equalToConstant: self.view.frame.width).isActive = true
            formReporte.viewFecha.fieldFechaHora.frame = CGRect(x: 15, y: 0, width:  (self.view.frame.width / 10) * 8,  height: 40)
            formReporte.viewFecha.dateButton.addTarget(self, action: #selector(datePickerShow), for: UIControlEvents.touchUpInside)
            
            formReporte.viewAssets.widthAnchor.constraint(equalToConstant: self.view.frame.width - 30).isActive = true
            formReporte.viewAssets.cameraButton.addTarget(self, action: #selector(accessCamera), for: UIControlEvents.touchUpInside)
            formReporte.viewAssets.pictureButton.addTarget(self, action: #selector(accessGalery), for: UIControlEvents.touchUpInside)
            formReporte.viewAssets.audioButton.addTarget(self, action: #selector(recordAudio(_ :)), for: UIControlEvents.touchUpInside)

            //agregan los constraints del formulario
            cell.addConstraint(NSLayoutConstraint(item: formReporte, attribute: .top, relatedBy: .equal, toItem: scrooView, attribute: .top, multiplier: 1, constant: 40))
            
            cell.addConstraint(NSLayoutConstraint(item: formReporte, attribute: .centerX, relatedBy: .equal, toItem: scrooView, attribute: .centerX, multiplier: 1, constant: 0))
            
            cell.addConstraint(NSLayoutConstraint(item: tableAss, attribute: .top, relatedBy: .equal, toItem: assetsView, attribute: .bottom, multiplier: 1, constant: 10))
            
            
        }else if indexPath.row == 1{
            cell.addSubview(tableReportes)
            cell.addSubview(buttonDeleteAll)
            
            tableReportes.tableFooterView = UIView()
            tableReportes.backgroundColor = .clear
            
            tableReportes.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
            tableReportes.heightAnchor.constraint(equalToConstant: view.frame.height - 150).isActive = true
            cell.addConstraint(NSLayoutConstraint(item: tableReportes, attribute: .top, relatedBy: .equal, toItem: cell, attribute: .top, multiplier: 1, constant: 65))
            
            cell.addConstraint(NSLayoutConstraint(item: buttonDeleteAll, attribute: .bottom, relatedBy: .equal, toItem: cell, attribute: .bottom, multiplier: 1, constant: -20))
            cell.addConstraint(NSLayoutConstraint(item: buttonDeleteAll, attribute: .right, relatedBy: .equal, toItem: cell, attribute: .right, multiplier: 1, constant: -20))
            
            
        }
        return cell
    }
    
    func setupAssetsView() {
        scrooView.addSubview(assetsView)
        assetsView.titleView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        /*assetsView.tableAssets.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        assetsView.tableAssets.heightAnchor.constraint(equalToConstant: 150).isActive = true
        assetsView.tableAssets.tableFooterView = UIView()
        assetsView.tableAssets.backgroundColor = .clear
        assetsView.tableAssets.separatorStyle = .none
        assetsView.tableAssets.register(AssetCell.self, forCellReuseIdentifier: "assetcell")
        assetsView.deleteButton = true
        assetsView.assets = nameAssets*/
        tableAss.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        tableAss.heightAnchor.constraint(equalToConstant: 150).isActive = true
        tableAss.tableFooterView = UIView()
        tableAss.backgroundColor = .clear
        tableAss.separatorStyle = .none
        tableAss.register(AssetCell.self, forCellReuseIdentifier: "assetcell")
        assetsView.frame = CGRect(x: 0, y: 570, width: formReporte.frame.width, height: 50)
        view.bringSubview(toFront: assetsView)
    }
    
    
    func datePickerSetup() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(setDate))
        toolbar.setItems([doneButton], animated: false)
        
        formReporte.viewFecha.fieldFechaHora.inputAccessoryView = toolbar
        formReporte.viewFecha.fieldFechaHora.inputView = datePicker
    }
    
    func datePickerShow() {
        
        formReporte.viewFecha.fieldFechaHora.becomeFirstResponder()
        
    }
    
    func setDate() {
        date = datePicker.date
        formReporte.viewFecha.fieldFechaHora.text = "\(date)"
        formReporte.viewFecha.fieldFechaHora.textColor = Constants().blueColor
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        formReporte.descriptionView.fieldWhite.resignFirstResponder()
        formReporte.viewLugar.fieldTransparent.resignFirstResponder()
        return true
    }
    
    func setupRecorder() {
        let recordSettings = [AVFormatIDKey : kAudioFormatAppleLossless,
                              AVEncoderAudioQualityKey : AVAudioQuality.max.rawValue,
                              AVEncoderBitRateKey : 320000,
                              AVNumberOfChannelsKey : 2,
                              AVSampleRateKey : 44100.0 ] as [String : Any]
        
        
        do {
            soudRecorder = try AVAudioRecorder(url: getAudioUrl(), settings: recordSettings)
            soudRecorder.delegate = self
            soudRecorder.prepareToRecord()
        } catch let error {
            print("error occured \(error)")
        }
        
    }
    
    func getCacheDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        return paths[0]
    }
    
    func getAudioUrl() -> URL{
        let path =  getCacheDirectory().appending("/assetAudio.m4a")
        let filePath = URL(fileURLWithPath: path)
        print(filePath.absoluteString)
        return filePath
    }
    
    func recordAudio(_ sender: UIButton) {
        if nameAssets.count >= 3 {
            let alView = SCLAlertView()
            alView.showError("No puedes agregar más de 3 elementos", subTitle: "", closeButtonTitle: "Aceptar")
        }else{
            soudRecorder.record()
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            
            let alertView = SCLAlertView(appearance: appearance)
            let alertViewIcon = UIImage(named: "ic_mic_black_24dp")
            alertView.addButton("Detener y Eliminar") {
                self.soudRecorder.stop()
            }
            
            alertView.addButton("Detener y Guardar") {
                self.soudRecorder.stop()
                self.addAudioAsset()
            }
            
            alertView.showInfo("Estas grabando", subTitle: "" ,circleIconImage: alertViewIcon)
        }
    }
    
    func preparePlayer() {
        do {
            soundPlayer = try AVAudioPlayer(contentsOf: getAudioUrl())
            soundPlayer.delegate = self
            soundPlayer.prepareToPlay()
            soundPlayer.volume = 10.0
        }catch let error{
            print("TENEMOS UN ERROR \(error)")
        }
    }
    
    func addAudioAsset() {
        let urlNew = getAudioUrl()
        nameAssets += ["assetAudio\(nameAssets.count + 1)"]
        
        urls += [urlNew]
        
        assetsView.assets = nameAssets
        tableAss.reloadData()
    }
    
    func accessLocation() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined, .restricted, .denied:
                locationManager = CLLocationManager()
                locationManager.delegate = self
                locationManager.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                let currentLocation = CLLocationManager().location
                
                latitude = String(describing: (currentLocation?.coordinate.latitude)!)
                longitude = String(describing: (currentLocation?.coordinate.longitude)!)
                
                formReporte.viewLugar.fieldTransparent.text = "\(latitude), \(longitude)"
            }
            
        } else {
            let appearance = SCLAlertView.SCLAppearance(
                showCircularIcon: true
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.showError("El GPS esta desactivado", subTitle: "Activa tu GPS e intentalo de nuevo")
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse{
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) {
                if CLLocationManager.isRangingAvailable() {
                    
                }
            }
        }
    }
    
    func accessCamera() {
        if nameAssets.count >= 3 {
            let alView = SCLAlertView()
            alView.showError("No puedes agregar más de 3 elementos", subTitle: "", closeButtonTitle: "Aceptar")
        }else{
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self as UIImagePickerControllerDelegate & UINavigationControllerDelegate
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera;
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func accessGalery() {
        if nameAssets.count >= 3 {
            let alView = SCLAlertView()
            alView.showError("No puedes agregar más de 3 elementos", subTitle: "", closeButtonTitle: "Aceptar")
        }else{
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary;
                imagePicker.allowsEditing = true
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        var nameImageAsset = ""
        
        let imagePickedData = UIImageJPEGRepresentation(image!, 0.5)
        
        if picker.sourceType == .camera {
            let imageData: Data = UIImagePNGRepresentation(image!)!
            let compressedImage = UIImage(data: imageData)
            let name = "asset.png"
            
            UIImageWriteToSavedPhotosAlbum(compressedImage!, nil, nil, nil)
            UserDefaults.standard.set(imageData, forKey: name)
            
            nameImageAsset = "assetCamera\(nameAssets.count + 1)"
            
        }else{
            nameImageAsset = "assetGalery\(nameAssets.count + 1)"
        }
        dataAssets.append(imagePickedData!)
        self.dismiss(animated: true, completion: nil)
        nameAssets.append(nameImageAsset)
        tableAss.reloadData()
    }
    
    func removeAsset(indice: Int) {
        nameAssets.remove(at: indice)
        urls.remove(at: indice)
    }
    
    func invalidDataReport (message: String) {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showError(message, subTitle: "")
    }
    
    func noConection () {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showError("No estas conectado a internet", subTitle: "Necesitas una conexión a internet para poder enviar reportes")
    }
    
    func saveReport() {
        if con == false {
            noConection()
        }else{
            if category == "" {
                invalidDataReport(message: "Selecciona una categoría válida")
            }else {
                let categorySelected = realm.objects(Category.self).filter("name = '\(category)'").first
                let muniSelected = realm.objects(Municipality.self).filter("name = '\(mun)'").first
                let departamentSelected = realm.objects(Department.self).filter("name = '\(dep)'").first
                
                
                let formatter = DateFormatter()
                
                formatter.dateFormat = "dd/MM/yyyy hh:mm a"
                let result = formatter.string(from: datePicker.date)
                
                let reports = realm.objects(Report.self)
                let newReport = Report()
                
                if let lastReport = reports.last {
                    newReport.id = (lastReport.id) + 1
                }else{
                    newReport.id = 1
                }
                newReport.category = categorySelected
                newReport.municipalidad = muniSelected
                newReport.department = departamentSelected
                newReport.textual = formReporte.descriptionView.fieldWhite.text!
                newReport.time = result
                newReport.lat = latitude
                newReport.lon = longitude
                newReport.ubucacion = formReporte.viewLugar.fieldTransparent.text!
                newReport.type = "REPORT"
                
                newR = newReport
                
                if let muniid = muniSelected?.id {
                    let params: Parameters = [
                        "sender":0,
                        "categorySender": (categorySelected?.id)!,
                        "textual":  formReporte.descriptionView.fieldWhite.text!,
                        "ubication": formReporte.viewLugar.fieldTransparent.text!,
                        "municipality": muniid,
                        "reportDatetime": result,
                        "lat": latitude,
                        "lon": longitude
                    ]
                    
                    Services().postReport(params: params, imageUrl: urls, report: newReport, dataAssets: dataAssets)
                    performSegue(withIdentifier: "resumeSave", sender: self)
                }else{
                    invalidDataReport(message: "Selecciona un municipio válido")
                }
            }
        }
    }
    
    func sendToDetalle(report: Report?) {
        reportToDetail = report!
        let filterRespuestas = "report.id = \(report!.id)"
        let answ = realm.objects(Respuesta.self).filter(filterRespuestas)
        
        for a in answ {
            respuestasCell += [a]
        }
        
        performSegue(withIdentifier: "reportToDetail", sender: self)
    }

    
    func sendToRespuesta(rs: Respuesta?) {
        performSegue(withIdentifier: "sendRespuesta", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "resumeSave" {
            if let destviewController : ResumenReporteController = segue.destination as? ResumenReporteController {
                destviewController.repo = self.newR
            }
            
            nameAssets = []
            urls = []
            dataAssets = []
            mun = ""
            dep = ""
            category = ""
            formReporte.categoryView.valueLabel.setTitle("Seleccione una Categoría", for: .normal)
            formReporte.departamentoView.valueLabel.setTitle("Seleccione un Departamento", for: .normal)
            formReporte.municipalidadView.valueLabel.setTitle("Seleccione un Municipio", for: .normal)
            formReporte.viewFecha.fieldFechaHora.text = ""
            formReporte.viewFecha.fieldFechaHora.placeholder = "Fecha"
            formReporte.viewLugar.fieldTransparent.text = ""
            formReporte.viewLugar.fieldTransparent.placeholder = "Lugar"
            tableAss.reloadData()
        }else if segue.identifier == "reportToDetail" {
            if let destViewController : DetalleReporteController = segue.destination as? DetalleReporteController{
                destViewController.report = reportToDetail
                destViewController.answers = respuestasCell
            }
        }else if segue.identifier == "sendToTutorial" {
            
            
            let lastPreference = realm.objects(Preference.self).first
            let pref = Preference()
            pref.sexo = (lastPreference?.sexo)!
            pref.edad = (lastPreference?.edad)!
            pref.hijos = (lastPreference?.hijos)!
            pref.departamento = lastPreference?.departamento
            pref.municipaidad = lastPreference?.municipaidad
            pref.anonimo = (lastPreference?.anonimo)!
            pref.uid = (lastPreference?.uid)!
            pref.registered = (lastPreference?.registered)!
            pref.tutorial = true

            try! realm.write {
                realm.add(pref, update: true)
            }
        }
    }
    
}
