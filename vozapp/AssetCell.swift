//
//  AssetCell.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/26/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import Foundation
import UIKit

class AssetCell: UITableViewCell {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        isUserInteractionEnabled = true
        setupView()
    }
    
    let buttonDelete : UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "deleteAsset"), for: .normal)
        btn.widthAnchor.constraint(equalToConstant: 25).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 25).isActive = true
        btn.layer.cornerRadius = 12.5
        btn.backgroundColor = Constants().yellowColor
        btn.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5)
        return btn
    }()
    
    let nameAsset : UILabel = {
        let label = UILabel()
        label.text = "nameAsset"
        label.textColor = Constants().blueColor
        return label
    }()
    
    func setupView() {
        addSubview(nameAsset)
        nameAsset.translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: nameAsset, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: nameAsset, attribute: .left, relatedBy: .equal, toItem: self, attribute: .left, multiplier: 1, constant: 10))
    }
    
    func addDeleteButton() {
        addSubview(buttonDelete)
        buttonDelete.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: buttonDelete, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: buttonDelete, attribute: .right, relatedBy: .equal, toItem: self, attribute: .right, multiplier: 1, constant: -10))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
