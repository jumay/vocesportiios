//
//  Report.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/23/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import RealmSwift
import Realm

class Report: Object {
    dynamic var id = Int()
    dynamic var rid = Int()
    dynamic var name = String()
    dynamic var category: Category!
    dynamic var textual = String()
    dynamic var ubucacion = String()
    dynamic var municipalidad: Municipality!
    dynamic var department: Department!
    dynamic var time = String()
    dynamic var lat = String()
    dynamic var lon = String()
    dynamic var type = String()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    func save() {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(self)
        }
    }
    
    func delete(id: String) {
        let realm = try! Realm()
        
        let filter = "id = \(id)"
        let element = realm.objects(Report.self).filter(filter).first
        
        if (element != nil) {
            try! realm.write {
                realm.delete(element!)
            }
        }
    }
}
