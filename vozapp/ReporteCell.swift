//
//  ReporteCell.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/31/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class ReporteCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCell()
        setupTableRespuestas()
    }
    
    var respuestas = [Respuesta]()
    
    let content : UIView = {
        let view = UIView()
        view.backgroundColor = Constants().yellowColor
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let readMoreLabel: UILabel = {
        let label = UILabel()
        label.text = "Leer Más"
        label.textColor = Constants().blueColor
        return label
    }()
    
    let readMoreButton : UIButton = {
        let btn = UIButton()
        btn.setImage(#imageLiteral(resourceName: "moreIcon"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        return btn
    }()
    
    let labelType: UILabel = {
        let label = UILabel()
        label.lineBreakMode = .byTruncatingMiddle
        label.numberOfLines = 0
        label.text = "Categoria del reporte"
        label.textColor = Constants().blueColor
        return label
    }()
    
    let fechaLabel: UILabel = {
        let label = UILabel()
        label.text = "Fecha de envio del reporte"
        label.textColor = Constants().blueColor
        return label
    }()
    
    let respuestasTable: UITableView = {
        let table = UITableView()
        table.tableFooterView = UIView()
        table.backgroundColor = .clear
        table.translatesAutoresizingMaskIntoConstraints = false
        table.isHidden = true //esto se tiene que quitar por el diseño nuevo de las respuestas
        return table
    }()
    
    let titleRespuestas: UILabel = {
        let label = UILabel()
        label.text = "Respuestas"
        label.textColor = .white
        return label
    }()
    
    func setupCell() {
        addSubview(content)
        //content.addSubview(readMoreLabel)
        content.addSubview(labelType)
        content.addSubview(fechaLabel)
        content.addSubview(readMoreButton)
    
        //readMoreLabel.translatesAutoresizingMaskIntoConstraints = false
        labelType.translatesAutoresizingMaskIntoConstraints = false
        fechaLabel.translatesAutoresizingMaskIntoConstraints = false
        
        labelType.widthAnchor.constraint(equalToConstant: 250).isActive = true
        //readMoreLabel.heightAnchor.constraint(equalToConstant: self.frame.height).isActive = true
        //addConstraint(NSLayoutConstraint(item: readMoreLabel, attribute: .right, relatedBy: .equal, toItem: content, attribute: .right, multiplier: 1, constant: -15))
        //addConstraint(NSLayoutConstraint(item: readMoreLabel, attribute: .top, relatedBy: .equal, toItem: content, attribute: .top, multiplier: 1, constant: 10))
        
        addConstraint(NSLayoutConstraint(item: fechaLabel, attribute: .top, relatedBy: .equal, toItem: content, attribute: .top, multiplier: 1, constant: 13))
        addConstraint(NSLayoutConstraint(item: labelType, attribute: .top, relatedBy: .equal, toItem: fechaLabel, attribute: .bottom, multiplier: 1, constant: 3))
        
        
        addConstraint(NSLayoutConstraint(item: labelType, attribute: .left, relatedBy: .equal, toItem: content, attribute: .left, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: fechaLabel, attribute: .left, relatedBy: .equal, toItem: content, attribute: .left, multiplier: 1, constant: 10))
        
        addConstraint(NSLayoutConstraint(item: readMoreButton, attribute: .right, relatedBy: .equal, toItem: content, attribute: .right, multiplier: 1, constant: -10))
        addConstraint(NSLayoutConstraint(item: readMoreButton, attribute: .bottom, relatedBy: .equal, toItem: content, attribute: .bottom, multiplier: 1, constant: -10))
        
        
        
    }
    
    func setupTableRespuestas() {
        addSubview(respuestasTable)
        respuestasTable.delegate = self
        respuestasTable.dataSource = self
        
        respuestasTable.register(RespuestaCell.self, forCellReuseIdentifier: "respuestaCell")
        
        addConstraint(NSLayoutConstraint(item: respuestasTable, attribute: .top, relatedBy: .equal, toItem: fechaLabel, attribute: .top, multiplier: 1, constant: 80))
        
    }
    
    func setupTitleRespuestas () {
        addSubview(titleRespuestas)
        
        addConstraint(NSLayoutConstraint(item: titleRespuestas, attribute: .top, relatedBy: .equal, toItem: fechaLabel, attribute: .bottom, multiplier: 1, constant: 60))
        addConstraint(NSLayoutConstraint(item: titleRespuestas, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "respuestaCell") as! RespuestaCell
        cell.backgroundColor = UIColor(red: 125/255.0, green: 193/255.0, blue: 255/255.0, alpha: 0.4)
        let data = respuestas[indexPath.row]
        cell.fechaLabel.text = data.time
        cell.labelType.text = data.textual
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return respuestas.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    var reportesController: ReportesController?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let respuesta = respuestas[indexPath.row]
        Globals.respuestaId = respuesta.id
        
        print(Globals.respuestaId)
        reportesController?.sendToRespuesta(rs: respuesta)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
