//
//  Globals.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/29/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import Foundation

struct Globals {
    static var respuestaId = 0
    static var token = ""
}
