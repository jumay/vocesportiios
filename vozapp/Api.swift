//
//  Api.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/17/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import Alamofire

class Api {
    
    typealias jsonStandard = [String: AnyObject]
    
    func get(url: String) -> jsonStandard{
        var res = jsonStandard()
        Alamofire.request(url, method: .get).responseJSON(completionHandler: {
            response in

            res = self.parseData(JSONData: response.data!)
            
        })
        return res
    }
    
    func post(url: String, params: Parameters) -> jsonStandard {
        var res = jsonStandard()
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            response in
            res = self.parseData(JSONData: response.data!)
            
        })
        return res
    }
    
    func parseData(JSONData: Data) -> jsonStandard{
        var res = jsonStandard()
        do{
            let readableJson = try JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers) as! jsonStandard
            res = readableJson
        }catch {
            print(error)
        }
        
        return res
    }
}
