//
//  EstablecimientoCell.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/29/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class EstablecimientoCell: UITableViewCell {

    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var content: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
