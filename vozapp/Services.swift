//
//  Services.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/25/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import Alamofire
import RealmSwift
import Realm

class Services {
    typealias jsonStandard = [String: AnyObject]
    let baseUrl = Constants().url
    let realm = try! Realm()
    var reportesController: ReportesController?
    var tutorialController: ViewController?
    
    func getDepartments() {
        self.tutorialController?.chargeCatalogs()
        
        let entriPoint = "/departments"
        let url = "\(baseUrl)\(entriPoint)"
        Alamofire.request(url, method: .get).responseJSON(completionHandler: {
            response in

            let res = self.parseData(JSONData: response.data!)
            if res.count > 0{
                let data = res[0] as! NSArray
                
                for i in 0..<data.count {
                    let dep = data[i] as! NSDictionary
                    
                    let newDep = Department()
                    newDep.name = dep["name"]! as! String
                    newDep.id = dep["id"]! as! Int
                    if let latitude = dep["latitude"]! as? String {
                        newDep.latitude = latitude
                    }
                    
                    if let longitude = dep["longitude"]! as? String {
                        newDep.longitude =  longitude
                    }
                    
                    newDep.save()
                }
            }
        })
        
    }
    
    func getMuns() {
        let entriPoint = "/municipalities"
        let url = "\(baseUrl)\(entriPoint)"
        
        Alamofire.request(url, method: .get).responseJSON(completionHandler: {
            response in
            
            let res = self.parseData(JSONData: response.data!)
            if res.count > 0 {
                let data = res[0] as! NSArray
                
                for i in 0..<data.count {
                    let mun = data[i] as! NSDictionary
                    let depMun = mun["department"]! as! NSDictionary
                    let filter = "id = \(depMun["id"]! as! Int)"
                    
                    let newMun = Municipality()
                    
                    if let dep = self.realm.objects(Department.self).filter(filter).first {
                        newMun.department = dep
                    }
                    
                    newMun.name = mun["name"]! as! String
                    newMun.id = mun["id"]! as! Int
                    if let latitude = mun["latitude"]! as? String {
                        newMun.latitude = latitude
                    }
                    
                    if let longitude = mun["longitude"]! as? String {
                        newMun.longitude =  longitude
                    }
                    
                    newMun.save()
                }
            }
        })
    }
    
    func getAges() {
        let entriPoint = "/age-ranges"
        let url = "\(baseUrl)\(entriPoint)"
        print(url)
        
        Alamofire.request(url, method: .get).responseJSON(completionHandler: {
            response in
            print(response)
            let res = self.parseData(JSONData: response.data!)
            if res.count > 0{
                let data = res[0] as! NSArray
                
                for i in 0..<data.count {
                    let age = data[i] as! NSDictionary
                    
                    let newAge = Age()
                    
                    newAge.id = age["id"]! as! Int
                    newAge.value = age["name"]! as! String
                    newAge.save()
                }
                
                self.tutorialController?.stopLoad()
            }
        })
    }
    
    
    func postUser() {
        let entriPoint = "/external-users"
        let url = "\(baseUrl)\(entriPoint)"
        
        print(url)
        
        let pref = realm.objects(Preference.self).first
        let filterAge = "value = '\((pref?.edad)!)'"
        let age = realm.objects(Age.self).filter(filterAge).first
        
        var arraySchool = [String]()
        let escuelas = pref?.escuelas
        for escuela in escuelas! {
            arraySchool += [escuela.name]
        }
        
        var sexo = ""
        if pref?.sexo == "Masculino" {
            sexo = "M"
        }else{
            sexo = "F"
        }
        
        var name = ""
        var lastname = ""
        var phone = ""
        if pref?.anonimo == false {
            name = (pref?.nombres)!
            lastname = (pref?.apellidos)!
            phone = (pref?.telefono)!
        }
        
        let params: Parameters = [
            "ageRange":(age?.id)!,
            "sex":sexo,
            "hasChildren":false,
            "department":(pref?.departamento?.id)!,
            "municipality":(pref?.municipaidad?.id)!,
            "schools": arraySchool,
            "firstNames": name,
            "lastNames": lastname,
            "phone": phone
        ]
        
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON(completionHandler: {
            response in
            
            let res = self.parseData(JSONData: response.data!)
            if res.count > 0{
                let data = res[0] as! NSDictionary
                
                let preferences = self.realm.objects(Preference.self)
                
                let preferNew = Preference()
                
                if pref?.anonimo == false {
                    preferNew.nombres = (pref?.nombres)!
                    preferNew.apellidos = (pref?.apellidos)!
                    preferNew.telefono = (pref?.telefono)!
                }
                
                if let newId = data["id"] {
                    preferNew.sexo = preferences[0].sexo
                    preferNew.edad = preferences[0].edad
                    preferNew.hijos = preferences[0].hijos
                    preferNew.departamento = preferences[0].departamento
                    preferNew.municipaidad = preferences[0].municipaidad
                    preferNew.uid = newId as! Int
                    preferNew.anonimo = (pref?.anonimo)!
                    preferNew.registered = true
                    
                    try! self.realm.write {
                        self.realm.add(preferNew, update: true)
                    }
                    
                    if Globals.token == "" {
                        self.getToken()
                    }
                }
            }
        })
    }
    
    
    func postReport(params: Parameters, imageUrl: [URL], report: Report, dataAssets: [Data]) {
        let entriPoint = "/reports"
        let url = "\(baseUrl)\(entriPoint)"
        
        let pref = realm.objects(Preference.self).first
        let pr: Parameters = [
            "sender":(pref?.uid)!,
            "categorySender": params["categorySender"]!,
            "textual":  params["textual"]!,
            "ubication": params["ubication"]!,
            "municipality": params["municipality"]!,
            "reportDatetime": params["reportDatetime"]!,
            "lat": params["lat"]!,
            "lon": params["lon"]!
        ]
        
        let header : HTTPHeaders = [
            "Auth-Token" : Globals.token
        ]
        
        Alamofire.request(url, method: .post, parameters: pr, encoding: JSONEncoding.default, headers: header).responseJSON(completionHandler: {
            response in
            
            
            let res = self.parseData(JSONData: response.data!)
            
                let data = res[0] as! NSDictionary
            
                    report.rid = data["rid"]! as! Int
            
                    print(report)
            
                    report.save()
                    self.postPicture(path: imageUrl, rid: data["rid"] as! Int, assets: dataAssets)
            
        })
    }
    
    func postPicture (path: [URL], rid: Int, assets: [Data]) {
        let entriPoint = "/reports"
        let url = "\(baseUrl)\(entriPoint)/\(rid)/upload"
        
        for asset in assets {
            let fileNmae = "assetIos.jpeg"
            let file = "file"
            
            let headers: HTTPHeaders = [
                "Content-Disposition": "[form-data; name=\(file); filename=\(fileNmae)]",
                "Accept-Encoding": "multipart/form-data",
                "Auth-Token" : Globals.token
            ]
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(asset, withName: file, fileName: fileNmae, mimeType: "image/jpeg")
                    //multipartFormData.append(pathImage, withName: file, fileName: nameF, mimeType: "image/jpeg")
            },
                to: url,
                method: .post,
                headers: headers,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response)
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        print("fallo el envio")
                    }
            }
            )
            
        }
        
        for p in path {
            let fileNmae = "audioIOS.m4a"
            let file = "file"
            
            let headers: HTTPHeaders = [
                "Content-Disposition": "[form-data; name=\(file); filename=\(fileNmae)]",
                "Accept-Encoding": "multipart/form-data",
                "Auth-Token" : Globals.token
            ]
            
            Alamofire.upload(
                multipartFormData: { multipartFormData in
                    multipartFormData.append(p, withName: file, fileName: fileNmae, mimeType: "media/m4a")
                    //multipartFormData.append(pathImage, withName: file, fileName: nameF, mimeType: "image/jpeg")
            },
                to: url,
                method: .post,
                headers: headers,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            debugPrint(response)
                        }
                    case .failure(let encodingError):
                        print(encodingError)
                        print("fallo el envio")
                    }
            }
            )
        }
    }
    
    func postAudio (path: URL) {
        let entriPoint = "/reports"
        let rId = 58
        let url = "\(baseUrl)\(entriPoint)/\(rId)/upload"
        
        let file = "imageIOS\(rId)"
        let fname = "imageIOS\(rId)"
        
        let headers: HTTPHeaders = [
            "Content-Disposition": "[form-data; name='\(file)'; filename='\(fname)']",
            "Accept-Encoding": "multipart/form-data"
        ]
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append(path, withName: file, fileName: fname, mimeType: "audio/mp3")
        },
            to: url,
            method: .post,
            headers: headers,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseJSON { response in
                        debugPrint(response)
                    }
                case .failure(let encodingError):
                    print(encodingError)
                }
        }
        )
    }
    
    func getCategories() {
        let entriPoint = "/categories"
        let url = "\(baseUrl)\(entriPoint)"
        print(url)
        let header : HTTPHeaders = [
            "Auth-Token" : Globals.token
        ]
        
        let existCategories = realm.objects(Category.self)
        
        Alamofire.request(url, method: .get, headers: header).responseJSON(completionHandler: {
            response in
            
            let res = self.parseData(JSONData: response.data!)
            
            if res.count > 0 {
                let data = res[0] as! NSArray
                
                if data.count > 0{
                    try! self.realm.write {
                        self.realm.delete(existCategories)
                    }
                    
                    for i in 0..<data.count {
                        let category = data[i] as! NSDictionary
                        
                        let newCategory = Category()
                        
                        newCategory.id = category["id"]! as! Int
                        newCategory.name = category["name"]! as! String
                        newCategory.save()
                    }
                }
                
                self.reportesController?.stopLoad()
            }
        })
    }
    
    func deleteReport(reportId: Int) {
        let pref = realm.objects(Preference.self).first
        let uid = pref?.uid
        let entriPoint = "/reports/\(reportId)"
        let url = "\(baseUrl)\(entriPoint)"
        
        let header : HTTPHeaders = [
            "Auth-Token" : Globals.token
        ]
        
        let params: Parameters = [
            "sender":uid!
        ]
        Alamofire.request(url, method: .delete, parameters: params, headers: header).responseJSON(completionHandler: {
            response in
            
            print(response)
        })
    }
    
    func deleteAllReport() {
        let pref = realm.objects(Preference.self).first
        let uid = pref?.uid
        let entriPoint = "/external-users/\(uid!)/reports"
        let url = "\(baseUrl)\(entriPoint)"
        
        let header : HTTPHeaders = [
            "Auth-Token" : Globals.token
        ]
        
        Alamofire.request(url, method: .delete, headers: header).responseJSON(completionHandler: {
            response in
            
            print(response)
        })
    }
    
    func getAnswers() {
        let pref = realm.objects(Preference.self).first
        let entriPoint = "/external-users/\((pref?.uid)!)/reports"
        
        let url = "\(baseUrl)\(entriPoint)"
        
        let header : HTTPHeaders = [
            "Auth-Token" : Globals.token
        ]
        
        if pref?.uid != 0 {
            Alamofire.request(url, method: .get, headers: header).responseJSON(completionHandler: {
                response in
                
                let res = self.parseDataFrontendService(JSONData: response.data!)
                let dataDetail = res
                
                for i in 0..<dataDetail.count {
                    let report = dataDetail[i] as! NSDictionary
                    
                    let id = report["id"] as! Int
                    let reportDB = self.realm.objects(Report.self).filter("rid = \(id)").first
                    
                    let files = report["files"] as! NSArray
                    let answers = report["answers"] as! NSArray
                    for f in 0..<files.count {
                        let file = files[f] as! NSDictionary
                        let newfile = File()
                        
                        newfile.id = file["fileId"] as! Int
                        newfile.filename = file["name"] as! String
                        newfile.url = file["url"] as! String
                        newfile.filetype = file["fileType"] as! String
                        newfile.report = reportDB
                        
                        newfile.save()
                    }
                    
                    if  answers.count > 0 {
                        let ans = self.realm.objects(Respuesta.self)
                        let files = self.realm.objects(File.self)
                        
                        if answers.count != ans.count{
                            try! self.realm.write {
                                self.realm.delete(ans)
                                self.realm.delete(files)
                            }
                            for e in 0..<answers.count {
                                let ans = answers[e] as! NSDictionary
                                
                                let newAnswer = Respuesta()
                                let ansFiles = ans["answerfile"] as! NSArray
                                
                                newAnswer.id = ans["id"] as! Int
                                newAnswer.textual = ans["textual"] as! String
                                newAnswer.title = ans["title"] as! String
                                newAnswer.time = ans["senton"] as! String
                                newAnswer.report = reportDB
                                newAnswer.save()
                                
                                for f in 0..<ansFiles.count {
                                    let file = ansFiles[f] as! NSDictionary
                                    let newfile = File()
                                    
                                    newfile.id = file["id"] as! Int
                                    newfile.filename = file["filename"] as! String
                                    newfile.url = file["url"] as! String
                                    newfile.filetype = file["fileType"] as! String
                                    newfile.answer = newAnswer
                                    
                                    newfile.save()
                                }
                                
                            }
                        }
                    }
                    
                }
            })
        }
    }
    
    
    func getToken() {
        self.reportesController?.chargeCatalogs()
        let pref = realm.objects(Preference.self).first
        
        if pref != nil {
            let entriPoint = "/users/login"
            let url = "\(baseUrl)\(entriPoint)"
            
            if let uid = pref?.uid{
                let agerangePref = pref?.edad
                let department = pref?.departamento?.id
                let municipality = pref?.municipaidad?.id
                let sexPref = pref?.sexo
                
                var sex = ""
                
                if sexPref == "Masculino" {
                    sex = "M"
                }else{
                    sex = "F"
                }
                
                let agerangeFilter = "value = '\(agerangePref!)'"
                let agerange = realm.objects(Age.self).filter(agerangeFilter).first
                
                
                let userpass = "\(agerange!.id),\(department!),\(municipality!),\(sex)"
                
                let params: Parameters = [
                    "userId" : "\(uid)",
                    "username": userpass,
                    "password": userpass
                ]
                
                Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON(completionHandler: {
                    response in
                    let res = self.parseData(JSONData: response.data!)
                    
                    if res.count > 0{
                        let newdata = res[0] as! NSDictionary
                        let newToken = newdata["token"] as! String
                        
                        Globals.token = newToken
                        if newToken != "" {
                            self.getData()
                        }
                    }else{
                        Globals.token = "noexiste"
                    }
                })
            }
        }
        
    }
    
    func getData() {
        let realm = try! Realm()
        let categories = realm.objects(Category.self)
        if categories.count <= 0 {
            self.getCategories()
        }else{
            self.reportesController?.stopLoad()
        }
        
        self.getAnswers()
        self.getCategories()
        
    }
    
    func parseData(JSONData: Data) -> NSArray{
        var res = NSArray()
        do{
            let readableJson = try JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers) as! jsonStandard
            print(readableJson)
            if let dataDetalle = readableJson["detalle"] as? NSArray {
                res = dataDetalle
            }
            print(res)
        }catch {
            print(error)
        }
        
        return res
    }
    
    func parseDataFrontendService(JSONData: Data) -> NSArray {
        var res = NSArray()
        
        do{
            let readableJson = try JSONSerialization.jsonObject(with: JSONData, options: .mutableContainers) as! jsonStandard
            
            print(readableJson)
            if let dataDetalle = readableJson["resultDetail"] as? NSDictionary {
                res = dataDetalle["detalle"] as! NSArray
            }
        }catch {
            print(error)
        }
        
        return res
    }
}
