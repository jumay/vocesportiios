//
//  RespuestaDetalleCell.swift
//  vozapp
//
//  Created by Jonathan Herrera on 7/19/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class RespuestaDetalleCell: UITableViewCell {

    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewContainer.layer.cornerRadius = 5
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
