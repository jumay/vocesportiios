//
//  ResumenReporteController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/1/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class ResumenReporteController: UIViewController {

    var repo = Report()
    
    @IBOutlet weak var categoriaLabel: UILabel!
    @IBOutlet weak var departamentoLabel: UILabel!
    @IBOutlet weak var municipalidadLabel: UILabel!
    @IBOutlet weak var gpsLabel: UILabel!
    @IBOutlet weak var fechaLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        successReport()
    }
    
    func successReport () {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showSuccess("Tu reporte se guardo con éxito", subTitle: "")
    }
    
    func setupView() {
        
        categoriaLabel.text = repo.category.name
        departamentoLabel.text = repo.department.name
        municipalidadLabel.text = repo.municipalidad.name
        if repo.lat != "" {
            gpsLabel.text = "Si"
        }else{
            gpsLabel.text = "No"
        }
        fechaLabel.text = repo.time
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }

}
