//
//  NuevoReporte.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/24/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class NuevoReporte: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    let categoryView: ViewForm = {
        let view = ViewForm()
        view.labelForm.text = "Categoria del hecho: "
        view.valueLabel.setTitle("Seleccione Categoría", for: .normal)
        view.backgroundColor = Constants().yellowColor
        view.layer.cornerRadius = 5
        return view
    }()
    
    let departamentoView: ViewForm = {
        let view = ViewForm()
        view.labelForm.text = "Departamento: "
        view.backgroundColor = Constants().yellowColor
        view.layer.cornerRadius = 5
        return view
    }()
    
    let municipalidadView: ViewForm = {
        let view = ViewForm()
        view.labelForm.text = "Municipio: "
        view.backgroundColor = Constants().yellowColor
        view.layer.cornerRadius = 5
        return view
    }()
    
    let descriptionView: ViewForm = {
        let view = ViewForm()
        view.labelForm.text = "Descripción: "
        return view
    }()
    
    let viewLugar: ViewFormTransparent = {
        let view = ViewFormTransparent()
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()
    
    let viewFecha: ViewFormTransparent = {
        let view = ViewFormTransparent()
        view.heightAnchor.constraint(equalToConstant: 40).isActive = true
        return view
    }()
    
    let viewAssets: ViewFormTransparent = {
        let view = ViewFormTransparent()
        view.heightAnchor.constraint(equalToConstant: 80).isActive = true
        return view
    }()
    
    func setupView() {
        addSubview(categoryView)
        addSubview(departamentoView)
        addSubview(municipalidadView)
        addSubview(descriptionView)
        addSubview(viewLugar)
        addSubview(viewFecha)
        addSubview(viewAssets)

        addConstraint(NSLayoutConstraint(item: categoryView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: categoryView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: departamentoView, attribute: .top, relatedBy: .equal, toItem: categoryView, attribute: .bottom, multiplier: 1, constant: 10))
        addConstraint(NSLayoutConstraint(item: departamentoView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: municipalidadView, attribute: .top, relatedBy: .equal, toItem: departamentoView, attribute: .bottom, multiplier: 1, constant: 10))
        
        addConstraint(NSLayoutConstraint(item: municipalidadView, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1, constant: 0))
        
        addConstraint(NSLayoutConstraint(item: descriptionView, attribute: .top, relatedBy: .equal, toItem: municipalidadView, attribute: .bottom, multiplier: 1, constant: 10))
        
        addConstraint(NSLayoutConstraint(item: viewLugar, attribute: .top, relatedBy: .equal, toItem: descriptionView, attribute: .bottom, multiplier: 1, constant: 70))
        
        addConstraint(NSLayoutConstraint(item: viewFecha, attribute: .top, relatedBy: .equal, toItem: viewLugar, attribute: .bottom, multiplier: 1, constant: 8))
        
        addConstraint(NSLayoutConstraint(item: viewAssets, attribute: .top, relatedBy: .equal, toItem: viewFecha, attribute: .bottom, multiplier: 1, constant: 10))

        
        categoryView.setImageDrop()
        categoryView.setValueLabel()
        departamentoView.setImageDrop()
        departamentoView.setValueLabel()
        municipalidadView.setImageDrop()
        municipalidadView.setValueLabel()
        descriptionView.setTextField()
        viewLugar.setTextField()
        viewFecha.setFechaField()
        viewAssets.setAssetsView()
        
        
    }
    
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
