//
//  DetalleReporteController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/1/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift
import Alamofire

class DetalleReporteController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var recibidoLabel: UILabel!
    @IBOutlet weak var respuestaLabel: UILabel!
    @IBOutlet weak var categoriaLabel: UILabel!
    @IBOutlet weak var departamentoLabel: UILabel!
    @IBOutlet weak var municipalidadLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var ubicacionLabel: UILabel!
    @IBOutlet weak var fechaHechoLabel: UILabel!
    
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var tableRespuestas: UITableView!
    @IBOutlet weak var viewRespuesta: UIView!
    
    let realm = try! Realm()
    override func viewDidLoad() {
        super.viewDidLoad()

        tableRespuestas.delegate = self
        tableRespuestas.dataSource = self
        setupView()
        getFiles()
    }
    
    var report = Report()
    var nameAssets = [String]()
    var filesTable = [File]()
    var answers = [Respuesta]()
    
    let assetsView : ViewAssets = {
        let view = ViewAssets()
        view.isHidden = true
        return view
    }()
    
    func getFiles() {
        let filter = "report.id = \(report.id)"
        let files = realm.objects(File.self).filter(filter)
        for file in files {
            nameAssets += [file.filename]
            filesTable += [file]
        }
        
        if nameAssets.count > 0 {
            setupAssetsView()
        }
    }
    
    func setupAssetsView() {
        //scrollView.addSubview(assetsView)
        assetsView.titleView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 50)
        assetsView.tableAssets.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        assetsView.tableAssets.heightAnchor.constraint(equalToConstant: 150).isActive = true
        assetsView.tableAssets.tableFooterView = UIView()
        assetsView.tableAssets.backgroundColor = .clear
        assetsView.tableAssets.register(AssetCell.self, forCellReuseIdentifier: "assetcell")
        assetsView.deleteButton = false
        assetsView.assets = nameAssets
        assetsView.files = filesTable
        assetsView.frame = CGRect(x: 0, y: 545, width: scrollView.frame.width, height: 225)
    }
    
    func setupView() {
        scrollView.delegate = self
        scrollView.frame = CGRect(x: 0, y: 65, width: view.frame.width, height: view.frame.height)
        scrollView.contentSize = CGSize(width: view.frame.width, height: 850)
        
        deleteButton.layer.cornerRadius = 0.5 * deleteButton.bounds.size.width
        deleteButton.clipsToBounds = true
        deleteButton.imageEdgeInsets = UIEdgeInsetsMake(12,17,12,17)

        view.backgroundColor = .white
        
        let filter = "report.id = \(report.id)"
        let respuestas = realm.objects(Respuesta.self).filter(filter)
        
        recibidoLabel.text = report.time
        if respuestas.count > 0 {
            respuestaLabel.text = "si"
        }else{
            respuestaLabel.text = "no"
        }
        categoriaLabel.text = report.category?.name
        departamentoLabel.text = report.department?.name
        municipalidadLabel.text = report.municipalidad?.name
        
        if report.textual != "" {
            descripcionLabel.text = report.textual
        }else{
            descripcionLabel.text = "--------"
        }
        
        if report.ubucacion != "" {
            ubicacionLabel.text = report.ubucacion
        }else{
            ubicacionLabel.text = "--------"
        }
        
        fechaHechoLabel.text = "--------"
        
        tableRespuestas.separatorStyle = .none
        tableRespuestas.tableFooterView = UIView()
        tableRespuestas.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableRespuestas?.dequeueReusableCell(withIdentifier: "cellRespuesta") as! RespuestaDetalleCell
        cell.selectionStyle = .none
        cell.tituloLabel.text = answers[indexPath.row].title
        cell.fechaLabel.text = answers[indexPath.row].time
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

    @IBAction func deleteAction(_ sender: Any) {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "dropdown_arrow")
        alertView.addButton("Eliminar") {
            Services().deleteReport(reportId: self.report.rid)
            try! self.realm.write {
                self.realm.delete(self.report)
            }
            
            self.performSegue(withIdentifier: "reportDeleted", sender: self)
        }
        
        alertView.showError("¿Seguro?", subTitle: "Al eliminar el reporte no podrás volver a tener acceso a el",closeButtonTitle: "Cancelar" ,circleIconImage: alertViewIcon)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
