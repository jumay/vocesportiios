//
//  ViewAssets.swift
//  vozapp
//
//  Created by Jonathan Herrera on 6/26/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class ViewAssets: UIView, UITableViewDelegate, UITableViewDataSource {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    var assets = [String]()
    var files = [File]()
    var deleteButton = false

    let titleView : UIView = {
        let view = UIView()
        view.backgroundColor = Constants().yellowColor
        return view
    }()
    
    let titleLabel : UILabel = {
        let label = UILabel()
        label.textColor = Constants().blueColor
        label.text = "Archivos adjuntos"
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let tableAssets : UITableView = {
        let table = UITableView()
        table.separatorStyle = .none
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    func setupView() {
        //addSubview(tableAssets)
        addSubview(titleView)
        addSubview(titleLabel)
        
        tableAssets.delegate = self
        tableAssets.dataSource = self
        
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerY, relatedBy: .equal, toItem: titleView, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: titleLabel, attribute: .centerX, relatedBy: .equal, toItem: titleView, attribute: .centerX, multiplier: 1, constant: 0))
        //addConstraint(NSLayoutConstraint(item: tableAssets, attribute: .top, relatedBy: .equal, toItem: titleView, attribute: .bottom, multiplier: 1, constant: 10))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "assetcell", for: indexPath) as! AssetCell
        cell.nameAsset.text = "Hola"
        cell.backgroundColor = .blue
        //cell.selectionStyle = .none
        if self.deleteButton == true {
            cell.addDeleteButton()
            //cell.buttonDelete.tag = indexPath.row
            cell.buttonDelete.isUserInteractionEnabled = true
            //cell.buttonDelete.addTarget(self, action: #selector(deleteAction(_:)), for: UIControlEvents.touchUpInside)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("asetsTableView")
        if self.deleteButton == false {
            let linkBase = Constants().filesUrl
            let urlFile = files[indexPath.row].url
            let defUrl = "\(linkBase)\(urlFile)"
            if let urlAsset = URL(string: defUrl) {
                UIApplication.shared.open(urlAsset, options: [:], completionHandler: nil)
            }
        }else{
            print("Hola mundo")
        }
    }
    
    func deleteAction(_ sender: UIButton) {
        assets.remove(at: sender.tag)
        tableAssets.reloadData()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
