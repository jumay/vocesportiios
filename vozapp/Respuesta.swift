//
//  Respuesta.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/31/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import Realm
import RealmSwift

class Respuesta: Object {
    dynamic var id = Int()
    dynamic var title = String()
    dynamic var textual = String()
    dynamic var time = String()
    dynamic var report: Report?
    
    func save() {
        let realm = try! Realm()
        
        try! realm.write {
            realm.add(self)
        }
    }
    
    func delete(id: String) {
        let realm = try! Realm()
        
        let filter = "id = \(id)"
        let element = realm.objects(Respuesta.self).filter(filter).first
        
        if (element != nil) {
            try! realm.write {
                realm.delete(element!)
            }
        }
    }
}
