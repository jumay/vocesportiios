//
//  InfoUserController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/11/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class InfoUserController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    let cellId = "CellInfo"
    var pickerSelected = 0
    let formData = ["Rango de Edad", "Sexo", "Tiene Hijos", "Departamento", "Municipio"]
    var rangoEdad = [String]()
    let sexo = ["Masculino", "Femenino"]
    let tieneHijos = ["Sí", "No"]
    var deps = [String]()
    var muns = [String]()
    
    var edad = String()
    var sexoSelected = String()
    var hijos = String()
    var dep = String()
    var mun = String()
    
    let realm = try! Realm()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let con = Connection().isInternetAvailable()
        if con == false {
            noConection()
        }
        
        setData()
        setupNavigationButtons()
        setupTableView()
        setupPickerView()
        //showAlert() // Alarma para pedir los datos generales del App
    }
    
    func noConection () {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showError("No estas conectado a internet", subTitle: "Necesitas una conexión a internet para poder registrarte")
    }
    
    func setData() {
        let departamentos = realm.objects(Department.self)
        let ages = realm.objects(Age.self)
        
        for dep in departamentos {
            deps += [dep.name]
        }
        
        for age in ages {
            rangoEdad += [age.value]
        }
    }
    
    let picker : pickerInfo = {
        let picker = pickerInfo()
        return picker
    }()
    
    let buttonSelect: UIButton = {
        let btn = UIButton()
        btn.setTitle("Seleccionar", for: .normal)
        btn.setTitleColor(.blue, for: .normal)
        btn.addTarget(self, action: #selector(actionSelect), for: UIControlEvents.touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    let selectPickView: UIView = {
        let viewSelect = UIView()
        viewSelect.backgroundColor = .white
        viewSelect.translatesAutoresizingMaskIntoConstraints = false
        return viewSelect
    }()
    
    func setupNavigationButtons() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Siguiente", style: UIBarButtonItemStyle.plain, target: self, action: #selector(sendToNewScreen))
    }
    
    func showAlert () {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "dropdown_arrow")
        alertView.showInfo("¿Puedes proporcionarnos la siguiente información?", subTitle: "Nos interesa saber qué grupos de la población utilizan esta aplicación",closeButtonTitle: "Aceptar" ,circleIconImage: alertViewIcon)
    }
    
    func showAlertDep() {
        let appearance = SCLAlertView.SCLAppearance(
            showCircularIcon: true
        )
        let alertView = SCLAlertView(appearance: appearance)
        alertView.showError("Tienes que seleccionar un municipio", subTitle: "")
        
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = .clear
    }
    
    func setupPickerView() {
        self.view.addSubview(picker)
        self.view.addSubview(selectPickView)
        selectPickView.addSubview(buttonSelect)
        
        buttonSelect.widthAnchor.constraint(equalToConstant: 100).isActive = true
        buttonSelect.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        picker.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        picker.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        selectPickView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        selectPickView.heightAnchor.constraint(equalToConstant: 45).isActive = true
        
        view.addConstraint(NSLayoutConstraint(item: picker, attribute: .top, relatedBy: .equal, toItem: tableView, attribute: .bottom, multiplier: 1, constant: -110))
        view.addConstraint(NSLayoutConstraint(item: selectPickView, attribute: .top, relatedBy: .equal, toItem: tableView, attribute: .bottom, multiplier: 1, constant: -110))
        
        selectPickView.addConstraint(NSLayoutConstraint(item: buttonSelect, attribute: .right, relatedBy: .equal, toItem: selectPickView, attribute: .right, multiplier: 1, constant: -10))
        selectPickView.addConstraint(NSLayoutConstraint(item: buttonSelect, attribute: .centerY, relatedBy: .equal, toItem: selectPickView, attribute: .centerY, multiplier: 1, constant: 0))
        
        selectPickView.isHidden = true
        picker.isHidden = true
    }
    
    func actionSelect(sender:UIButton!) {
        var pickerRow = 0
        let index = IndexPath(row: pickerSelected, section: 0)
        let cell = tableView.cellForRow(at: index) as! InfoCell
        let cellMun = tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! InfoCell
        let selectedRow = picker.selectedRow
        let pickerLenght = picker.data.count
        
        if selectedRow > pickerLenght {
            pickerRow = pickerLenght - 1
        }else{
            pickerRow = selectedRow
        }
        
        let newDat = picker.data[pickerRow]
        
        cell.selectedValue.text = newDat
        picker.selectedValue = "[Seleccionar]"
        
        if pickerSelected == 0 {
            edad = newDat
        }else if pickerSelected == 1 {
            sexoSelected = newDat
        }else if pickerSelected == 2{
            hijos = newDat
        }else if pickerSelected == 3 {
            
            if newDat != dep {
                let munEmpty = "Seleccione un Municipio"
                cellMun.selectedValue.text = munEmpty
            }
            
            dep = newDat
            
            muns = []
            let filter = "department.name = '\(dep)'"
            let municipios = realm.objects(Municipality.self).filter(filter)
            
            
            for mn in municipios {
                muns += [mn.name]
            }
        }else{
            mun = newDat
        }
        
        
        picker.isHidden = true
        selectPickView.isHidden = true
    }
    
    func sendToNewScreen() {
        
        if edad == "" || sexoSelected == "" || hijos == "" || dep == "" || mun == ""{
            let appearance = SCLAlertView.SCLAppearance(
                showCircularIcon: true
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.showError("Todos los campos son obligatorios", subTitle: "")
            
        }else{
            let preferences = realm.objects(Preference.self)
            let departmentPref = realm.objects(Department.self).filter("name = '\(dep)'").first
            let municipalityPref = realm.objects(Municipality.self).filter("name = '\(mun)'").first
            
            let pref = Preference()
            
            
            pref.edad = edad
            pref.sexo = sexoSelected
            pref.hijos = hijos
            pref.departamento = departmentPref
            pref.municipaidad = municipalityPref
            pref.registered = false
            
            if preferences.count <= 0 {
                pref.save()
            }else{
                try! realm.write {
                    realm.add(pref, update: true)
                }
            }
            performSegue(withIdentifier: "toEstablecimientos", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! InfoCell
        let text = formData[indexPath.row]
        cell.labelForm?.text = "\(text):"
        cell.selectedValue.text = "\(text)"
        cell.labelForm?.adjustsFontSizeToFitWidth = true
        cell.labelForm?.minimumScaleFactor = 10/UIFont.labelFontSize
        cell.dropdownImage?.image = #imageLiteral(resourceName: "dropdwonIcon")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! InfoCell
        cell.selectionStyle = .none
        pickerSelected = indexPath.row
        if indexPath.row == 0 {
            picker.data = rangoEdad
        }else if indexPath.row == 1 {
            picker.data = sexo
        }else if indexPath.row == 2{
            picker.data = tieneHijos
        }else if indexPath.row == 3 {
            picker.data = deps
        }else{
            picker.data = muns
        }
        
        if indexPath.row == 4 {
            if dep == "" {
                showAlertDep()
            }else{
                picker.reloadAllComponents()
                selectPickView.isHidden = false
                picker.isHidden = false
                cell.selectedValue.adjustsFontSizeToFitWidth = true
                cell.selectedValue.minimumScaleFactor = 10/UIFont.labelFontSize
            }
        }else{
            picker.reloadAllComponents()
            selectPickView.isHidden = false
            picker.isHidden = false
            cell.selectedValue.adjustsFontSizeToFitWidth = true
            cell.selectedValue.minimumScaleFactor = 10/UIFont.labelFontSize
        }
        
        
    }
}
