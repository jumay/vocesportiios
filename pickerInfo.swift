//
//  pickerInfo.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/18/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class pickerInfo: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {

    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        setupView()
    }
    
    var data = [String]()
    var selectedValue = String()
    var selectedRow = Int()
    
    func setupView() {
        self.delegate = self
        self.dataSource = self
        
        self.translatesAutoresizingMaskIntoConstraints = false
        showsSelectionIndicator = true
        backgroundColor = .white
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return data[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedValue = data[row]
        selectedRow = row
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
