//
//  InfoCell.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/11/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell {

    @IBOutlet weak var labelForm: UILabel!
    @IBOutlet weak var selectedValue: UILabel!
    @IBOutlet weak var dropdownImage: UIImageView!
    
    @IBOutlet weak var content: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupCell()
        
    }
    
    func setupCell() {
        content.layer.cornerRadius = 4
        
        dropdownImage.translatesAutoresizingMaskIntoConstraints = false
        selectedValue.translatesAutoresizingMaskIntoConstraints = false
        selectedValue.textColor = Constants().blueColor
        
        selectionStyle = .none
        
        dropdownImage.frame = CGRect(x: 0, y: 0, width: 40, height: 40)

        addConstraint(NSLayoutConstraint(item: dropdownImage, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))
        addConstraint(NSLayoutConstraint(item: selectedValue, attribute: .centerY, relatedBy: .equal, toItem: self, attribute: .centerY, multiplier: 1, constant: 0))

    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
