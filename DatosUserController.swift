//
//  DatosUserController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 5/23/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class DatosUserController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var apellidosField: UITextField!
    @IBOutlet weak var nombresField: UITextField!
    @IBOutlet weak var telefonoField: UITextField!
    
    let realm = try! Realm()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        apellidosField.delegate = self
        nombresField.delegate = self
        telefonoField.delegate = self
        
        setupView()
        setupNavigationButtons()
        addDoneButtonOnKeyboard()
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: 50) )
        
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Aceptar", style: UIBarButtonItemStyle.done, target: self, action: #selector(DatosUserController.doneButtonAction))
        
        let items = NSMutableArray()
        items.add(flexSpace)
        items.add(done)
        
        doneToolbar.items = items as? [UIBarButtonItem]
        doneToolbar.sizeToFit()
        
        telefonoField.inputAccessoryView = doneToolbar
        
    }
    
    func doneButtonAction()
    {
        telefonoField.resignFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setupView() {
    }
    
    
    func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 64{
                self.view.frame.origin.y -= (keyboardSize.height - 40)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            print(keyboardSize.height)
            if self.view.frame.origin.y != 64{
                self.view.frame.origin.y = 64
            }
        }
    }
    
    
    func setupNavigationButtons() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Siguiente", style: UIBarButtonItemStyle.plain, target: self, action: #selector(setupUserInformation))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        apellidosField.resignFirstResponder()
        nombresField.resignFirstResponder()
        telefonoField.resignFirstResponder()
        
        return true
    }
    
    func setupUserInformation() {
        let preferences = realm.objects(Preference.self)
        
        let pref = Preference()

        pref.nombres = nombresField.text!
        pref.apellidos = apellidosField.text!
        pref.telefono = telefonoField.text!
        
        if preferences.count <= 0 {
            pref.save()
        }else{
            pref.sexo = preferences[0].sexo
            pref.edad = preferences[0].edad
            pref.hijos = preferences[0].hijos
            pref.departamento = preferences[0].departamento
            pref.municipaidad = preferences[0].municipaidad
            pref.uid = preferences[0].uid
            pref.anonimo = false
            pref.registered = false
            try! realm.write {
                realm.add(pref, update: true)
            }
        }
        Services().postUser()
        performSegue(withIdentifier: "successRegister", sender: self)
    }
    
    
}
