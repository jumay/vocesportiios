//
//  TypeUserController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 7/18/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class TypeUserController: UIViewController {

    @IBOutlet weak var anonimoBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    
    
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func anonimo() {
        let preferences = realm.objects(Preference.self)
        let pref = Preference()
        pref.anonimo = true
        
        if preferences.count <= 0 {
            pref.save()
        }else{
            pref.sexo = preferences[0].sexo
            pref.edad = preferences[0].edad
            pref.hijos = preferences[0].hijos
            pref.departamento = preferences[0].departamento
            pref.municipaidad = preferences[0].municipaidad
            pref.uid = preferences[0].uid
            pref.registered = false
            try! realm.write {
                realm.add(pref, update: true)
            }
        }
        
        Services().postUser()

        
        performSegue(withIdentifier: "typeAnonimo", sender: self)
    }
    
    func informacionPersonal() {
        let preferences = realm.objects(Preference.self)
        let pref = Preference()
        pref.anonimo = false
        
        if preferences.count <= 0 {
            pref.save()
        }else{
            pref.sexo = preferences[0].sexo
            pref.edad = preferences[0].edad
            pref.hijos = preferences[0].hijos
            pref.departamento = preferences[0].departamento
            pref.municipaidad = preferences[0].municipaidad
            pref.uid = preferences[0].uid
            pref.registered = false
            try! realm.write {
                realm.add(pref, update: true)
            }
        }
        
        performSegue(withIdentifier: "typeRegistered", sender: self)
        
    }
    
    @IBAction func anonimoPressed(_ sender: Any) {
        anonimo()
    }

    @IBAction func registerPressed(_ sender: Any) {
        informacionPersonal()
    }
}
