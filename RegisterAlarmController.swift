//
//  RegisterAlarmController.swift
//  vozapp
//
//  Created by Jonathan Herrera on 7/18/17.
//  Copyright © 2017 Unicef Guatemala. All rights reserved.
//

import UIKit

class RegisterAlarmController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func okPressed(_ sender: Any) {
        performSegue(withIdentifier: "passToDashboard", sender: self)
    }
}
